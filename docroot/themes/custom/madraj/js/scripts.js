/**
 * @file
 * Behaviors for the madraj theme.
 */

(function ($, _, Drupal) {
  Drupal.behaviors.madraj = {
    attach: function () {
      const facts = $('.node--type-fact.node--view-mode-teaser').once();
      const numbers = facts.find('.field--name-field-number').text();
      console.log(numbers);

      let transits = new countUp.CountUp(numbers, 0);
      if (!transits.error) {
        transits.start();
      } else {
        console.error(transits.error);
      }
    }
  };
})(window.jQuery, window._, window.Drupal);
