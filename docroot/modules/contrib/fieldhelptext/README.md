The Field Help Text module provides a UI for bulk editing field help text. It
allows you to use a single form to edit the help text for all fields on a content
entity bundle simultaneously, or to edit one field's label and help text
everywhere the field is used.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/fieldhelptext

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/fieldhelptext
